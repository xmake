/******************************************************************************
 * @file            lib.c
 *****************************************************************************/
#include    <assert.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    <xmake/cstr.h>
#include    <xmake/lib.h>
#include    <xmake/report.h>
#include    <xmake/variable.h>
#include    <xmake/xmake.h>

size_t len = 0;

static int print_help (const char *arg) {

    (void) arg;
    
    fprintf (stderr, "Usage: %s [options] [target] ...\n\n", program_name);
    fprintf (stderr, "Options:\n\n");
    
    fprintf (stderr, "    -B                    Ignored.\n");
    fprintf (stderr, "\n");
    
    fprintf (stderr, "    -C DIRECTORY          Change to DIRECTORY before doing anything.\n");
    fprintf (stderr, "    -I DIRECTORY          Search DIRECTORY for included makefiles.\n");
    fprintf (stderr, "\n");
    
    fprintf (stderr, "    -f FILE               Read FILE as a makefile.\n");
    fprintf (stderr, "    -s                    Don't echo recipes.\n");
    fprintf (stderr, "\n");
    
    fprintf (stderr, "    --no-print-directory  Don't print the current directory.\n");
    fprintf (stderr, "    --help                Print this help information.\n");
    
    fprintf (stderr, "\n");
    exit (EXIT_SUCCESS);

}

static int print_version (const char *arg) {

    (void) arg;
    
    fprintf (stderr, "%s: version 0.0.1\n", program_name);
    return 1;

}

static int set_quiet (const char *arg) {

    (void) arg;
    
    state->quiet = 1;
    return 0;

}

static int set_no_print (const char *arg) {

    (void) arg;
    
    state->no_print = 1;
    return 0;

}

static int add_include_path (const char *path) {

    const char *in = path;
    const char *p;
    
    do {
    
        int c;
        
        CString str;
        cstr_new (&str);
        
        for (p = in; c = *p, c != '\0' && c != PATHSEP[0]; p++) {
        
            if (c == '\\') { c = '/'; }
            cstr_ccat (&str, c);
        
        }
        
        if (str.size) {
        
            if (((char *) str.data)[str.size - 1] != '/') {
                cstr_ccat (&str, '/');
            }
            
            cstr_ccat (&str, '\0');
            dynarray_add (&state->include_paths, &state->nb_include_paths, xstrdup (str.data));
        
        }
        
        cstr_free (&str);
        in = (p + 1);
    
    } while (*p != '\0');
    
    return 0;

}

static int ignored (const char *arg) {

    (void) arg;
    return 0;

}

static int add_directory (const char *path) {

    char *arg = xstrdup (path);
    
    while (arg[strlen (arg) - 1] == '/' || arg[strlen (arg) - 1] == '\\') {
        arg[strlen (arg) - 1] = '\0';
    }
    
    dynarray_add (&state->directories, &state->nb_directories, arg);
    return 0;

}

static int add_makefile (const char *name) {

    dynarray_add (&state->makefiles, &state->nb_makefiles, xstrdup (name));
    return 0;

}

static int non_option (const char *arg) {

    if (strchr (arg, '=')) {
    
        char *temp = xstrdup (arg);
        
        parse_var_line (temp, VAR_ORIGIN_COMMAND_LINE);
        free (temp);
    
    } else {
        dynarray_add (&state->goals, &state->nb_goals, xstrdup (arg));
    }
    
    return 0;

}

static struct option opts[] = {

    {   "--help",                   &print_help             },
    {   "-h",                       &print_help             },
    
    {   "--version",                &print_version          },
    
    {   "--silent",                 &set_quiet              },
    {   "--quiet",                  &set_quiet              },
    {   "-s",                       &set_quiet              },
    
    {   "--no-print-directory",     &set_no_print           },
    
    {   "--no-builtin-rules",       &ignored                },
    {   "-r",                       &ignored                },
    
    {   "--include_dir=",           &add_include_path       },
    {   "-I:",                      &add_include_path       },
    
    {   "--always-make",            &ignored                },
    {   "-B",                       &ignored                },
    
    {   "--directory=",             &add_directory          },
    {   "-C:",                      &add_directory          },
    
    {   "--makefile=",              &add_makefile           },
    {   "--file=",                  &add_makefile           },
    {   "-f:",                      &add_makefile           },
    
    {   0,                          &non_option             }

};

static int match_rule (const char *rule, const char *arg) {

    const char *ptr;
    
    assert (rule);
    assert (arg);
    
    while (*rule == *arg && *rule && *arg) {
    
        rule++;
        arg++;
    
    }
    
    switch (*rule) {
    
        case '\0':
            return *arg == '\0';
        
        case '[':
        
            ptr = ++rule;
            
            while (*ptr != ']') {
                ptr++;
            }
            
            return !strncmp (rule, arg, ptr - rule) ? match_rule (ptr + 1, arg + (ptr - rule)) : match_rule (ptr + 1, arg);
        
        case '{':
        
            do {
            
                ptr = ++rule;
                
                while (*ptr != '}' && *ptr != '|') {
                    ptr++;
                }
                
                if (strncmp (rule, arg, ptr - rule) == 0) {
                
                    arg = arg + (ptr - rule);
                    
                    do {
                    
                        rule = ptr;
                        ptr++;
                    
                    } while (*rule != '}');
                    
                    return match_rule (ptr, arg);
                
                }
                
                rule = ptr;
            
            } while (*rule == '|');
            
            break;
    
    }
    
    return 0;

}

static int match_arg (struct option opt, int argc, char **argv, int *ret) {

    size_t rule_len, arg_len;
    assert (opt.callback);
    
    *ret = 0;
    
    rule_len = strlen (opt.rule);
    arg_len = strlen (argv[0]);
    
    switch (opt.rule[rule_len - 1]) {
    
        case ':':
        
            rule_len--;
            
            if (strncmp (opt.rule, argv[0], rule_len) == 0) {
            
                if (arg_len == rule_len) {
                
                    if (argc < 2) {
                    
                        report_at (program_name, 0, REPORT_ERROR, "missing argument to '%s'", opt.rule);
                        
                        *ret = 1;
                        return 0;
                    
                    }
                    
                    *ret = opt.callback (argv[1]);
                    return 2;
                
                } else {
                
                    assert (arg_len > rule_len);
                    
                    *ret = opt.callback (argv[0] + rule_len);
                    return 1;
                
                }
            
            }
            
            break;
        
        case '<':
        
            rule_len--;
            /* fall through */
        
        case '=':
        
            if (strncmp (opt.rule, argv[0], rule_len) == 0) {
            
                if (arg_len == rule_len) {
                
                    report_at (program_name, 0, REPORT_ERROR, "missing argument to '%s'", argv[0]);
                    
                    *ret = 1;
                    return 0;
                
                }
                
                *ret = opt.callback (argv[0] + rule_len);
                return 1;
            
            }
            
            break;
        
        default:
        
            if (match_rule (opt.rule, argv[0])) {
            
                *ret = opt.callback (argv[0]);
                return 1;
            
            }
    
    }
    
    return 0;

}


char *xstrdup (const char *__s) {

    char *p = xmalloc (strlen (__s) + 1);
    
    strcpy (p, __s);
    return p;

}

char *xstrndup (const char *__s, unsigned long __len) {

    char *p = xmalloc (__len + 1);
    
    memcpy (p, __s, __len);
    return p;

}

int parse_args (char **__args, int __cnt) {

    struct option *opt, *last;
    int i, c, ret;
    
    for (last = opts; last->rule; last++) {
        ;
    }
    
    for (i = 1; i < __cnt; ) {
    
        if (*(__args[i]) == '-') {
        
            for (opt = opts, c = 0; opt->rule && !c; opt++) {
            
                c = match_arg (*opt, __cnt - i, __args + i, &ret);
                
                if (ret != 0) {
                    return ret;
                }
            
            }
            
            if (c) {
                i += c;
            } else {
            
                report_at (program_name, 0, REPORT_ERROR, "unrecognized option '%s'", __args[i]);
                return 1;
            
            }
        
        } else {
        
            if ((ret = last->callback (__args[i])) != 0) {
                return ret;
            }
            
            i++;
        
        }
    
    }
    
    if (!state->nb_makefiles) {
        dynarray_add (&state->makefiles, &state->nb_makefiles, xstrdup ("Makefile"));
    }
    
    return 0;

}

void dynarray_add (void *__ptab, unsigned long *__nb_ptr, void *__data) {

    long nb, nb_alloc;
    void **pp;
    
    nb = *__nb_ptr;
    pp = *(void ***) __ptab;
    
    if ((nb & (nb - 1)) == 0) {
    
        if (!nb) {
            nb_alloc = 1;
        } else {
            nb_alloc = nb * 2;
        }
        
        pp = xrealloc (pp, nb_alloc * sizeof (void *));
        *(void ***) __ptab = pp;
    
    }
    
    pp[nb++] = __data;
    *__nb_ptr = nb;

}

void *xmalloc (unsigned long __sz) {

    void *ptr = malloc (__sz);
    
    if (!ptr && __sz) {
    
        report_at (program_name, 0, REPORT_ERROR, "memory full (malloc)");
        exit (EXIT_FAILURE);
    
    }
    
    memset (ptr, 0, __sz);
    return ptr;

}

void *xrealloc (void *__ptr, unsigned long __sz) {

    void *ptr = realloc (__ptr, __sz);
    
    if (!ptr && __sz) {
    
        report_at (program_name, 0, REPORT_ERROR, "memory full (realloc)");
        exit (EXIT_FAILURE);
    
    }
    
    return ptr;

}


#if     defined(_WIN32)
# include   <windows.h>

char *get_current_directory (void) {

    static TCHAR tszBuffer[4096];
    size_t i;
    
    DWORD dwRet;
    
    if ((dwRet = GetCurrentDirectory (sizeof (tszBuffer), tszBuffer)) == 0) {
    
        report_at (program_name, 0, REPORT_FATAL_ERROR, "failed to get current directory");
        exit (EXIT_FAILURE);
    
    }
    
    for (i = 0; i < strlen (tszBuffer); i++) {
    
        if (tszBuffer[i] == '\\') {
            tszBuffer[i] = '/';
        }
    
    }
    
    return tszBuffer;

}

int change_directory (const char *__path) {
    return (SetCurrentDirectory (__path) != 0);
}

int directory_exists (const char *__path) {

    DWORD dwAttrib = GetFileAttributes (__path);
    return ((dwAttrib != -1LU) && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));

}
#else
# include   <dirent.h>
# include   <unistd.h>

char *get_current_directory (void) {

    static char cwd[4096] = { 0 };
    memset (cwd, 0, sizeof (cwd));
    
    if (getcwd (cwd, sizeof (cwd))) {
        return cwd;
    }
    
    report_at (program_name, 0, REPORT_FATAL_ERROR, "failed to get current directory");
    exit (EXIT_FAILURE);

}

int change_directory (const char *__path) {
    return (chdir (__path) == 0);
}

int directory_exists (const char *__path) {

    DIR *dir;
    
    if ((dir = opendir (__path))) {
    
        closedir (dir);
        return 1;
    
    }
    
    return 0;

}
#endif
