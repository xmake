/******************************************************************************
 * @file            read.h
 *****************************************************************************/
#ifndef     _READ_H
#define     _READ_H

int read_makefile (const char *filename);

struct nameseq {

    char *name;
    struct nameseq *next;

};

void *parse_nameseq (char *line, size_t size);
void record_files (struct nameseq *filenames, char *cmds, size_t cmds_idx, char *depstr);

#endif      /* _READ_H */
