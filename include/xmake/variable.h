/******************************************************************************
 * @file            variable.h
 *****************************************************************************/
#ifndef     _VARIABLE_H
#define     _VARIABLE_H

enum variable_flavor {

    VAR_FLAVOR_RECURSIVELY_EXPANDED,
    VAR_FLAVOR_SIMPLY_EXPANDED,
    VAR_FLAVOR_IMMEDIATELY_EXPANDED

};

enum variable_origin {

    VAR_ORIGIN_AUTOMATIC,
    VAR_ORIGIN_COMMAND_LINE,
    VAR_ORIGIN_FILE

};

struct variable {

    char *name, *value;
    
    enum variable_flavor flavor;
    enum variable_origin origin;

};

struct variable *variable_add (char *name, char *value, enum variable_origin origin);
struct variable *variable_change (char *name, char *value, enum variable_origin origin);
struct variable *variable_find (char *name);

char *variable_expand_line (char *line);

void parse_var_line (char *line, enum variable_origin origin);
void variables_init (void);

#endif      /* _VARIABLE_H */
