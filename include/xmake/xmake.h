/******************************************************************************
 * @file            xmake.h
 *****************************************************************************/
#ifndef     _XMAKE_H
#define     _XMAKE_H

struct xmake_state {

    char **makefiles;
    unsigned long nb_makefiles;
    
    char **goals;
    unsigned long nb_goals;
    
    char **include_paths;
    unsigned long nb_include_paths;
    
    char **directories;
    unsigned long nb_directories;
    
    char *path;
    int quiet, no_print;

};

extern struct xmake_state *state;
extern const char *program_name;

int rule_search_and_build (char *name);

#endif      /* _XMAKE_H */
