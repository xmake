/******************************************************************************
 * @file            lib.h
 *****************************************************************************/
#ifndef     _LIB_H
#define     _LIB_H

#if     defined (_WIN32)
# define    PATHSEP                     ";"
#else
# define    PATHSEP                     ":"
#endif

struct option {

    const char *rule;
    int (*callback) (const char *);

};

char *xstrdup (const char *__s);
char *xstrndup (const char *__s, unsigned long __len);

int parse_args (char **__args, int __cnt);
void dynarray_add (void *__ptab, unsigned long *__nb_ptr, void *__data);

void *xmalloc (unsigned long __sz);
void *xrealloc (void *__ptr, unsigned long __sz);


char *get_current_directory (void);

int change_directory (const char *__path);
int directory_exists (const char *__path);

#endif      /* _LIB_H */
