/******************************************************************************
 * @file            rule.h
 *****************************************************************************/
#ifndef     _RULE_H
#define     _RULE_H

#include    <xmake/command.h>
#include    <xmake/dep.h>

struct rule {

    char *name;
    
    struct dep *deps;
    struct commands *cmds;

};

struct suffix_rule {

    char *first, *second;
    struct commands *cmds;
    
    struct suffix_rule *next;

};

extern struct suffix_rule *suffix_rules;
struct rule *rule_find (const char *name);

void rule_add (char *name, struct dep *deps, struct commands *cmds);
void rule_add_suffix (char *name, struct commands *cmds);
void rules_init (void);

#endif      /* _RULE_H */
