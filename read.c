/******************************************************************************
 * @file            read.c
 *****************************************************************************/
#include    <ctype.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    <xmake/command.h>
#include    <xmake/dep.h>
#include    <xmake/lib.h>
#include    <xmake/read.h>
#include    <xmake/rule.h>
#include    <xmake/variable.h>
#include    <xmake/xmake.h>

extern struct variable *default_goal_var;

struct linebuf {

    char *start, *memory;
    unsigned long size;
    
    FILE *f;

};

static char *memory_fgets (char *str, int num, char **source_p) {

    char *source = *source_p;
    char *p = strchr (source, '\n');
    
    if (source[0] == '\0') {
        return 0;
    }
    
    if (p && p - source < num - 1) {
    
        memcpy (str, source, p - source + 1);
        str[p - source + 1] = '\0';
        
        *source_p += p - source + 1;
        return str;
    
    }
    
    if ((int) strlen (source) > num - 1) {
    
        memcpy (str, source, num - 1);
        str[num - 1] = '\0';
        
        *source_p += num - 1;;
        return str;
    
    }
    
    strcpy (str, source);
    
    *source_p += strlen (source);
    return str;

}

static long read_line (struct linebuf *lbuf) {

    long lines_read = 0;
    
    char *p = lbuf->start;
    char *end = lbuf->start + lbuf->size;
    
    while (lbuf->f ? fgets (p, end - p, lbuf->f) : memory_fgets (p, end - p, &(lbuf->memory))) {
    
        size_t offset;
        
        p += strlen (p);
        offset = p - lbuf->start;
        
        lines_read++;
        
        if (offset >= 2) {
        
            if (p[-1] == '\n' && p[-2] != '\\') {
            
                p[-1] = '\0';
                break;
            
            }
        
        } else if (offset >= 1) {
        
            if (p[-1] == '\n') {
            
                p[-1] = '\0';
                break;
            
            }
        
        }
        
        if (end - p >= 80) {
            continue;
        }
        
        lbuf->size *= 2;
        lbuf->start = xrealloc (lbuf->start, lbuf->size);
        
        p = lbuf->start + offset;
        end = lbuf->start + lbuf->size;
    
    }
    
    return lines_read;

}

static void remove_backslash_newlines (char *line) {

    char *in = line, *out = line;
    char *p;
    
    if (!(p = strchr (in, '\n'))) {
        return;
    }
    
    do {
    
        size_t out_line_len = p - in - 1;
        
        if (out != in) {
            memmove (out, in, out_line_len);
        }
        
        out += out_line_len;
        in = p + 1;
        
        while (isspace ((int) *in)) {
            in++;
        }
        
        *(out++) = ' ';
    
    } while ((p = strchr (in, '\n')));
    
    memmove (out, in, strlen (in) + 1);

}

static void remove_comment (char *line) {

    char *p;
    
    if ((p = strchr (line, '#'))) {
        *p = '\0';
    }

}

static int include_makefile (const char *filename) {

    char *path, *new_name;
    unsigned long i;
    
    if (!read_makefile (filename)) {
        return 0;
    }
    
    for (i = 0; i < state->nb_include_paths; i++) {
    
        path = state->include_paths[i];
        
        new_name = xmalloc (strlen (path) + strlen (filename) + 1);
        sprintf (new_name, "%s%s", path, filename);
        
        if (!read_makefile (new_name)) {
        
            free (new_name);
            return 0;
        
        }
        
        free (new_name);
    
    }
    
    fprintf (stderr, "%s: *** Failed to include '%s'. Stop.\n", program_name, filename);
    return 1;

}

static int read_lbuf (struct linebuf *lbuf, int set_default) {

    struct nameseq* filenames = 0;
    int ret;
    
    char *cmds, *clean = 0, *depstr = 0, *q;
    char *colon, *semicolonp, *commentp;
    
    long lines_read;
    size_t clean_size = 0, cmds_idx = 0, cmds_sz = 256;
    
    cmds = xmalloc (cmds_sz);
    
#define     record_waiting_files()                                              \
    do {                                                                        \
        if (filenames) {                                                        \
            record_files (filenames, cmds, cmds_idx, depstr);                   \
            filenames = 0;                                                      \
        }                                                                       \
        cmds_idx = 0;                                                           \
    } while (0)
    
    while ((lines_read = read_line (lbuf))) {
    
        char *line = lbuf->start;
        char *p, *src, *dst;
        
        if (line[0] == '\0') {
            continue;
        }
        
        if (line[0] == ' ' || line[0] == '\t') {
        
            if (filenames) {
            
                while (line[0] == ' ' || line[0] == '\t') {
                    line++;
                }
                
                if (cmds_idx + strlen (line) + 1 > cmds_sz) {
                
                    cmds_sz = (cmds_idx + strlen (line) + 1) * 2;
                    cmds = xrealloc (cmds, cmds_sz);
                
                }
                
                src = line;
                dst = &cmds[cmds_idx];
                
                for (; *src; src++, dst++) {
                
                    if (src[0] == '\n' && src[-1] == '\\' && src[1] == '\t') {
                    
                        *dst = *src;
                        
                        src++;
                        continue;
                    
                    }
                    
                    *dst = *src;
                
                }
                
                *dst = '\0';
                
                cmds_idx += dst - &cmds[cmds_idx] + 1;
                cmds[cmds_idx - 1] = '\n';
                
                continue;
            
            }
        
        }
        
        if (strlen (line) + 1 > clean_size) {
        
            clean_size = strlen (line) + 1;
            
            free (clean);
            clean = xmalloc (clean_size);
        
        }
        
        strcpy (clean, line);
        
        remove_backslash_newlines (clean);
        remove_comment (clean);
        
        p = clean;
        
        while (isspace ((int) *p)) {
            p++;
        }
        
        if (*p == '\0') {
            continue;
        }
        
        if (strncmp (p, "include", 7) == 0 && (isspace ((int) p[7]) || p[7] == '\0')) {
        
            p += 7;
            
            for (q = p + strlen (p); q > p && isspace ((int) q[-1]); q--) {
                ;
            }
            
            *q = '\0';
            p = line = variable_expand_line (xstrdup (p));
            
            while (1) {
            
                char saved_ch;
                
                for (; isspace ((int) *p); p++) {
                    ;
                }
                
                for (q = p; *q && !isspace ((int) *q); q++) {
                    ;
                }
                
                if (q == p) {
                    break;
                }
                
                saved_ch = *q;
                *q = '\0';
                
                if ((ret = include_makefile (p))) {
                    return ret;
                }
                
                *q = saved_ch;
                
                p = q;
            
            }
            
            free (line);
            continue;
        
        }
        
        if (strchr (p, '=')) {
        
            record_waiting_files ();
            
            parse_var_line (p, VAR_ORIGIN_FILE);
            continue;
        
        }
        
        record_waiting_files ();
        
        semicolonp = strchr (line, ';');
        commentp = strchr (line, '#');
        
        if (commentp && semicolonp && (commentp < semicolonp)) {
        
            *commentp = '\0';
            semicolonp = 0;
        
        } else if (semicolonp) {
            *(semicolonp++) = '\0';
        }
        
        remove_backslash_newlines (line);
        line = variable_expand_line (xstrdup (line));
        
        if (!(colon = strchr (line, ':'))) {
        
            for (p = line; isspace ((int) *p); p++) {
                ;
            }
            
            if (*p) {
                fprintf (stderr, "%s: Missing ':' in rule line!\n", program_name);
            }
            
            free (line);
            continue;
        
        }
        
        *colon = '\0';
        
        filenames = parse_nameseq (line, sizeof (*filenames));
        depstr = xstrdup (colon + 1);
        
        free (line);
        
        if (semicolonp) {
        
            if (cmds_idx + strlen (semicolonp) + 1 > cmds_sz) {
            
                cmds_sz = (cmds_idx + strlen (semicolonp) + 1) * 2;
                cmds = xrealloc (cmds, cmds_sz);
            
            }
            
            memcpy (&(cmds[cmds_idx]), semicolonp, strlen (semicolonp) + 1);
            cmds_idx += strlen (semicolonp) + 1;
        
        }
        
        if (set_default && default_goal_var->value[0] == '\0') {
        
            struct nameseq *ns;
            
            for (ns = filenames; ns; ns = ns->next) {
            
                if ((ns->name[0] == '.') && (strchr (ns->name, '\\') == 0) && (strchr (ns->name, '/') == 0)) {
                    continue;
                }
                
                free (default_goal_var->value);
                
                default_goal_var->value = xstrdup (ns->name);
                break;
            
            }
        
        }
    
    }
    
    record_waiting_files ();
    
    free (clean);
    free (cmds);
    
    return 0;

}

int read_makefile (const char *filename) {

    struct linebuf lbuf;
    struct variable *makefile_list;
    
    char *new_value;
    int ret;
    
    if (!(lbuf.f = fopen (filename, "r"))) {
        return 1;
    }
    
    lbuf.size = 256;
    lbuf.start = xmalloc (lbuf.size);
    
    if ((makefile_list = variable_find ("MAKEFILE_LIST"))) {
    
        unsigned long old_len = strlen (makefile_list->value);
        
        new_value = xmalloc (old_len + 1 + strlen (filename) + 1);
        sprintf (new_value, "%s %s", makefile_list->value, filename);
        
        variable_change ("MAKEFILE_LIST", new_value, VAR_ORIGIN_FILE);
    
    } else {
    
        new_value = xmalloc (strlen (filename) + 1);
        sprintf (new_value, "%s", filename);
        
        variable_change ("MAKEFILE_LIST", new_value, VAR_ORIGIN_FILE);
    
    }
    
    ret = read_lbuf (&lbuf, 1);
    
    free (lbuf.start);
    fclose (lbuf.f);
    
    return ret;

}

void *parse_nameseq (char *line, size_t size) {

    struct nameseq *start = 0;
    struct nameseq **pp = &start;
    
    char *p, *temp;
    temp = xmalloc (strlen (line) + 1);
    
#define     add_nameseq(_name)                              \
    do {                                                    \
        *pp = xmalloc (size);                               \
        (*pp)->name = xstrdup (_name);                      \
        pp = &((*pp)->next);                                \
    } while (0)
    
    p = line;
    
    while (1) {
    
        char *p2;
        
        while (isspace ((int) *p)) {
            p++;
        }
        
        if (*p == '\0') {
            break;
        }
        
        p2 = p;
        
        while (*p2 && !isspace ((int) *p2)) {
            p2++;
        }
        
        memcpy (temp, p, p2 - p);
        temp[p2 - p] = '\0';
        
        add_nameseq (temp);
        p = p2;
    
    }
    
#undef      add_nameseq
    
    free (temp);
    return start;

}

void record_files (struct nameseq *filenames, char *cmds, size_t cmds_idx, char *depstr) {

    struct commands *cmds_p;
    
    struct dep *deps;
    struct nameseq *ns, *old_ns;
    
    if (cmds_idx > 0) {
    
        cmds_p = xmalloc (sizeof (*cmds_p));
        
        cmds_p->text = xstrndup (cmds, cmds_idx);
        cmds_p->len = cmds_idx;
    
    } else {
        cmds_p = 0;
    }
    
    if (depstr) {
        deps = parse_nameseq (depstr, sizeof (*deps));
    } else {
        deps = 0;
    }
    
    for (ns = filenames, old_ns = 0; ns; old_ns = ns, ns = ns->next, free (old_ns->name), free (old_ns)) {
    
        if (ns->name[0] == '.' && !strchr (ns->name, '\\') && !strchr (ns->name, '/')) {
            rule_add_suffix (ns->name, cmds_p);
        } else {
            rule_add (ns->name, deps, cmds_p);
        }
    
    }
    
    free (depstr);

}
