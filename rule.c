/******************************************************************************
 * @file            rule.c
 *****************************************************************************/
#include    <string.h>

#include    <xmake/command.h>
#include    <xmake/dep.h>
#include    <xmake/hashtab.h>
#include    <xmake/lib.h>
#include    <xmake/rule.h>

static struct hashtab hashtab_rules = { 0 };
struct suffix_rule *suffix_rules = 0;

struct rule *rule_find (const char *name) {

    struct hashtab_name *key;
    
    if (!(key = hashtab_get_key (&hashtab_rules, name))) {
        return 0;
    }
    
    return hashtab_get (&hashtab_rules, key);

}

void rule_add (char *name, struct dep *deps, struct commands *cmds) {

    struct hashtab_name *key;
    struct rule *r;
    
    if (!(key = hashtab_alloc_name (xstrdup (name)))) {
        return;
    }
    
    r = xmalloc (sizeof (*r));
    
    r->name = xstrdup (name);
    r->deps = deps;
    r->cmds = cmds;
    
    hashtab_put (&hashtab_rules, key, r);

}

void rule_add_suffix (char *name, struct commands *cmds) {

    struct suffix_rule *s = xmalloc (sizeof (*s));
    char *p;
    
    if ((p = strchr (name + 1, '.'))) {
    
        s->second = xstrdup (p);
        *p = '\0';
    
    }
    
    s->first = xstrdup (name);
    
    s->cmds = cmds;
    s->next = suffix_rules;
    
    suffix_rules = s;

}

void rules_init (void) {}
