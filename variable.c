/******************************************************************************
 * @file            variable.c
 *****************************************************************************/
#include    <ctype.h>
#include    <stdio.h>
#include    <stdlib.h>
#include    <string.h>

#include    <xmake/hashtab.h>
#include    <xmake/lib.h>
#include    <xmake/report.h>
#include    <xmake/variable.h>
#include    <xmake/xmake.h>

static struct hashtab hashtab_vars = { 0 };

static char *variable_suffix_replace (char *body, const char *from_s, const char *to_s) {

    char *new_body = xstrdup (body);
    char *p;
    
    while ((p = strstr (new_body, from_s))) {
    
        if (strlen (from_s) == strlen (to_s)) {
            memcpy (p, to_s, strlen (to_s));
        } else if (strlen (from_s) > strlen (to_s)) {
        
            size_t rem = strlen (from_s) - strlen (to_s);
            memcpy (p, to_s, strlen (to_s));
            
            while (rem--) {
                p[strlen (to_s) + rem] = ' ';
            }
        
        } else {
        
            size_t rem = strlen (to_s) - strlen (from_s);
            
            new_body = xrealloc (new_body, strlen (new_body) + rem);
            memmove (p + rem, p, strlen (p) + 1);
            memcpy (p, to_s, strlen (to_s));
        
        }
    
    }
    
    return new_body;

}

struct variable *variable_add (char *name, char *value, enum variable_origin origin) {

    struct hashtab_name *key;
    struct variable *var;
    
    if (!(key = hashtab_alloc_name (name))) {
    
        report_at (program_name, 0, REPORT_ERROR, "failed to allocate memory for name '%s'", name);
        exit (EXIT_FAILURE);
    
    }
    
    var = xmalloc (sizeof (*var));
    var->flavor = VAR_FLAVOR_RECURSIVELY_EXPANDED;
    var->name = name;
    var->origin = origin;
    var->value = value;
    
    if (hashtab_put (&hashtab_vars, key, var)) {
    
        report_at (program_name, 0, REPORT_FATAL_ERROR, "failed to insert variable '%s' into hashtab", var->name);
        exit (EXIT_FAILURE);
    
    }
    
    return var;

}

struct variable *variable_change (char *name, char *value, enum variable_origin origin) {

    struct variable *var;
    
    if (!(var = variable_find (name))) {
        return variable_add (xstrdup (name), value, origin);
    }
    
    switch (origin) {
    
        case VAR_ORIGIN_AUTOMATIC:
        case VAR_ORIGIN_COMMAND_LINE:
        
            break;
        
        case VAR_ORIGIN_FILE:
        
            if (var->origin == VAR_ORIGIN_FILE) {
                break;
            }
            
            free (value);
            return 0;
    
    }
    
    free (var->value);
    
    var->value = value;
    var->origin = origin;
    
    return var;

}

struct variable *variable_find (char *name) {

    struct hashtab_name *key;
    
    if (!(key = hashtab_get_key (&hashtab_vars, name))) {
        return 0;
    }
    
    return hashtab_get (&hashtab_vars, key);

}

char *variable_expand_line (char *line) {

    size_t pos = 0;
    
    while (line[pos]) {
    
        if (line[pos] == '$') {
        
            char *new, *replacement = "";
            char *p_after_variable;
            
            struct variable *var = 0;
            char *alloc_replacement = 0;
            
            if (line[pos + 1] == '$') {
            
                p_after_variable = line + pos + 2;
                pos += 1;
            
            } else if (line[pos + 1] == '(' || line[pos + 1] == '{') {
            
                char *body = line + pos + 2;
                char *q = body, *content;
                
                int paren_inbalance = 1;
                char opening_paren = line[pos + 1];
                
                while (paren_inbalance) {
                
                    if (*q == opening_paren) {
                        paren_inbalance++;
                    } else if (*q == ')' && opening_paren == '(') {
                        paren_inbalance--;
                    } else if (*q == '}' && opening_paren == '{') {
                        paren_inbalance--;
                    } else if (*q == '\0') {
                    
                        fprintf (stderr, "%s: *** unterminated variable reference. Stop.\n", program_name);
                        exit (EXIT_FAILURE);
                    
                    }
                    
                    q++;
                
                }
                
                q--;
                
                p_after_variable = q + 1;
                content = variable_expand_line (xstrndup (body, q - body));
                
                if ((q = strchr (content, '='))) {
                
                    char *colon = strchr (content, ':');
                    
                    if (colon && colon != content && colon < q) {
                    
                        char *from, *to, *p;
                        *colon = '\0';
                        
                        var = variable_find (content);
                        
                        for (from = colon + 1; isspace ((int) *from); from++) {
                            ;
                        }
                        
                        for (p = q; p != from && isspace ((int) p[-1]); p--) {
                            ;
                        }
                        
                        *p = '\0';
                        
                        for (to = q + 1; isspace ((int) *to); to++) {
                            ;
                        }
                        
                        for (p = to + strlen (to); p != to && isspace ((int) p[-1]); p--) {
                            ;
                        }
                        
                        *p = '\0';
                        
                        if (*from && var) {
                            alloc_replacement = variable_suffix_replace (var->value, from, to);
                        }
                    
                    }
                
                } else {
                    var = variable_find (content);
                }
                
                free (content);
            
            } else if (line[pos + 1]) {
            
                char name[2] = { 0, 0 };
                
                p_after_variable = line + pos + 2;
                name[0] = line[pos + 1];
                
                var = variable_find (name);
            
            } else {
                p_after_variable = line + pos + 1;
            }
            
            if (var) {
                replacement = var->value;
            }
            
            if (alloc_replacement) {
                replacement = alloc_replacement;
            }
            
            new = xmalloc (pos + strlen (replacement) + strlen (p_after_variable) + 1);
            
            memcpy (new, line, pos);
            memcpy (new + pos, replacement, strlen (replacement));
            memcpy (new + pos + strlen (replacement), p_after_variable, strlen (p_after_variable) + 1);
            
            free (line);
            line = new;
            
            if (!alloc_replacement && var && var->flavor == VAR_FLAVOR_SIMPLY_EXPANDED) {
                pos += strlen (replacement);
            }
            
            free (alloc_replacement);
            continue;
            
        
        }
        
        pos++;
    
    }
    
    return line;

}

void parse_var_line (char *line, enum variable_origin origin) {

    enum {
        VAR_ASSIGN,
        VAR_CONDITIONAL_ASSIGN,
        VAR_APPEND,
        VAR_SHELL
    } opt = VAR_ASSIGN;
    
    enum variable_flavor flavor = VAR_FLAVOR_RECURSIVELY_EXPANDED;
    struct variable *var;
    
    char *var_name, *new_value;
    char *equals_sign, *p;
    
    if (!(equals_sign = strchr (line, '='))) {
    
        fprintf (stderr, "+++ invalid variable definition!\n");
        return;
    
    }
    
    p = equals_sign;
    
    switch (p - line) {
    
        default:
        
            if (p[-1] == ':' && p[-2] == ':' && p[-3] == ':') {
            
                flavor = VAR_FLAVOR_IMMEDIATELY_EXPANDED;
                
                p -= 3;
                break;
            
            }
            
            /* fall through */
        
        case 2:
        
            if (p[-1] == ':' && p[-2] == ':') {
            
                flavor = VAR_FLAVOR_SIMPLY_EXPANDED;
                
                p -= 2;
                break;
            
            }
            
            /* fall through */
        
        case 1:
        
            if (p[-1] == ':') {
            
                flavor = VAR_FLAVOR_SIMPLY_EXPANDED;
                
                p--;
                break;
            
            }
            
            if (p[-1] == '?') {
            
                opt = VAR_CONDITIONAL_ASSIGN;
                
                p--;
                break;
            
            }
            
            if (p[-1] == '+') {
            
                opt = VAR_APPEND;
                
                p--;
                break;
            
            }
            
            if (p[-1] == '!') {
            
                opt = VAR_SHELL;
                
                p--;
                break;
            
            }
            
            break;
        
        case 0:
        
            break;
    
    }
    
    for (; p > line && isspace ((int) p[-1]); p--) {
        ;
    }
    
    var_name = variable_expand_line (xstrndup (line, p - line));
    
    if (*var_name == '\0') {
    
        fprintf (stderr, "%s: *** empty variable name. Stop.\n", program_name);
        exit (EXIT_FAILURE);
    
    }
    
    var = variable_find (var_name);
    
    if (opt == VAR_CONDITIONAL_ASSIGN && var) {
    
        free (var_name);
        return;
    
    }
    
    for (p = equals_sign; isspace ((int) p[1]); p++) {
        ;
    }
    
    new_value = xstrdup (p + 1);
    
    if (opt == VAR_ASSIGN || opt == VAR_CONDITIONAL_ASSIGN) {
    
        switch (flavor) {
        
            case VAR_FLAVOR_RECURSIVELY_EXPANDED:
            
                break;
            
            case VAR_FLAVOR_SIMPLY_EXPANDED:
            
                new_value = variable_expand_line (new_value);
                break;
            
            case VAR_FLAVOR_IMMEDIATELY_EXPANDED: {
            
                size_t dollar_count;
                char *temp, *p2;
                
                new_value = variable_expand_line (new_value);
                
                for (dollar_count = 0, p = new_value; *p; p++) {
                
                    if (*p == '$') {
                        dollar_count++;
                    }
                
                }
                
                temp = xmalloc (strlen (new_value) + 1 + dollar_count);
                
                for (p = new_value, p2 = temp; *p; p++, p2++) {
                
                    *p2 = *p;
                    
                    if (*p == '$') {
                    
                        p2[1] = '$';
                        p2++;
                    
                    }
                    
                    *p2 = '\0';
                    
                    free (new_value);
                    new_value = temp;
                
                }
                
                break;
            
            }
        
        }
    
    } else if (opt == VAR_APPEND) {
    
        struct variable *var;
        
        if ((var = variable_find (var_name))) {
        
            char *temp = xstrdup (new_value);
            free (new_value);
            
            new_value = xmalloc (strlen (var->value) + 1 + strlen (temp) + 1);
            sprintf (new_value, "%s %s", var->value, temp);
            
            free (temp);
        
        }
    
    } else if (opt == VAR_SHELL) {
    
        fprintf (stderr, "+++internal error: != not supported yet. Stop.\n");
        exit (EXIT_FAILURE);
    
    }
    
    if ((var = variable_change (var_name, new_value, origin))) {
        var->flavor = flavor;
    }
    
    free (var_name);

}

void variables_init (void) {}
